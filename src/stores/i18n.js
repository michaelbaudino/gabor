import { writable, derived } from "svelte/store"

import en from "../data/i18n/en.json"
import fr from "../data/i18n/fr.json"

const dictionaries = { en, fr }
const defaultLocale = "en"

export const currentLocale = writable(defaultLocale)
export const i18n = derived(currentLocale, $currentLocale => dictionaries[$currentLocale])

export const formatNumber = derived(currentLocale, $currentLocale =>
  number => Intl.NumberFormat($currentLocale).format(number)
)

export const availableLocales = Object.keys(dictionaries).map(locale => ({
  locale: locale,
  label: dictionaries[locale].ui.localeLabel,
}))
