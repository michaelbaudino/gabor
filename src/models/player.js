import { writable, derived, get } from "svelte/store"

import rulebookRosters from "../data/rulebook-rosters.js"

export default class Player {
  constructor(race, initial = {}) {
    this.race = race
    this.name = writable(initial.name || "")
    this.positionId = writable(initial.positionId || undefined)
    this.additionalSkills = writable(initial.additionalSkills || [])
  }

  // Public Data Manipulation

  addSkill(skillToAdd) {
    this.additionalSkills.update(previousState => [...previousState, skillToAdd])
  }

  removeSkill(skillToRemove) {
    this.additionalSkills.update(previousState =>
      previousState.filter(skill => skill !== skillToRemove)
    )
  }

  // Public Stores

  get hasSkill() {
    return derived([this.defaultSkills, this.additionalSkills], ([$defaultSkills, $additionalSkills]) =>
      skill => [...$defaultSkills, ...$additionalSkills].includes(skill)
    )
  }

  get defaultSkills() {
    return derived(this.positionId, $positionId => {
      if (!$positionId) return []
      return this._rosterPosition($positionId).defaultSkills
    })
  }

  get availableSkillCategories() {
    return derived(this.positionId, $positionId => {
      if (!$positionId) return []
      return [
        ...this._rosterPosition($positionId).primarySkillCategories,
        ...this._rosterPosition($positionId).secondarySkillCategories,
      ]
    })
  }

  get characteristics() {
    return derived(this.positionId, $positionId => {
      if (!$positionId) return {}
      return this._rosterPosition($positionId).characteristics
    })
  }

  get cost() {
    return derived(this.positionId, $positionId => {
      if (!$positionId) return 0
      return this._rosterPosition($positionId).cost
    })
  }

  // Public Import/Export

  toJSON() {
    return {
      name: get(this.name),
      positionId: get(this.positionId),
      additionalSkills: get(this.additionalSkills),
    }
  }

 // Private methods

 _rosterPosition(positionId) {
    return rulebookRosters[this.race].positions[positionId]
 }
}
