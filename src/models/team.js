import { writable, derived, get } from "svelte/store"

import { urlFragmentData, watchable, onMetaStoreChange } from "../utils.js"

import { i18n } from "../stores/i18n.js"

import rulebookRosters from "../data/rulebook-rosters.js"

import Player from "./player.js"

export default class Team {
  defaults = {
    name: "",
    coach: "",
    race: Object.keys(rulebookRosters)[0],
    rerollCount: 0,
    apothecary: false,
    players: [],
  }

  apothecaryCost = 50000

  constructor() {
    // Public stores
    this.name = writable(this.defaults.name)
    this.coach = writable(this.defaults.coach)
    this.race = watchable(writable, this.defaults.race, (_) => this.players.set([]))
    this.rerollCount = writable(this.defaults.rerollCount)
    this.apothecary = writable(this.defaults.apothecary)
    this.players = writable(this.defaults.players)

    // Private stores
    this._cost = writable(0)
    this._positionAvailabilities = writable({})

    // Metastore subscriptions
    onMetaStoreChange(this._costMetaStore, cost => this._cost.set(cost))
    onMetaStoreChange(this._positionAvailabilitiesMetaStore, positionAvailabilities => this._positionAvailabilities.set(positionAvailabilities))

    // Load existing team
    this.loadFromUrl() || this.loadFromBrowser()
  }

  reset({ askConfirmation } = {}) {
    if (askConfirmation && !confirm(get(i18n).ui.confirmLoad)) return

    this._set(this.defaults)
    return true
  }

  // Public Data Manipulation

  addPlayer(initial = undefined) {
    this.players.update(previousState =>
      [...previousState, new Player(get(this.race), initial)]
    )
  }

  removePlayer(playerToRemove) {
    this.players.update(previousState =>
      previousState.filter(player => player !== playerToRemove)
    )
  }

  // Public Read-Only Stores

  get cost() {
    return derived(this._cost, $cost => $cost)
  }

  get positionAvailabilities() {
    return derived(this._positionAvailabilities, $positionAvailabilities => $positionAvailabilities)
  }

  get rulebookRoster() {
    return derived(this.race, $race => rulebookRosters[$race])
  }

  get rerollCost() {
    return derived(this.rulebookRoster, $rulebookRoster => $rulebookRoster.rerollCost)
  }

  // Public Serialization

  toJSON() {
    return {
      name: get(this.name),
      coach: get(this.coach),
      race: get(this.race),
      players: get(this.players).map(player => player.toJSON()),
      rerollCount: get(this.rerollCount),
      apothecary: get(this.apothecary),
    }
  }

  // Public Persistence

  saveInBrowser({ askConfirmation } = {}) {
    if (localStorage.getItem("gabor") && askConfirmation && !confirm(get(i18n).ui.confirmSave)) return

    localStorage.setItem("gabor", JSON.stringify(this))
    return true
  }

  get canLoadFromBrowser() {
    return localStorage.getItem("gabor") !== null
  }

  loadFromBrowser({ askConfirmation } = {}) {
    if (!localStorage.getItem("gabor")) return
    if (askConfirmation && !confirm(get(i18n).ui.confirmLoad)) return

    this._set(JSON.parse(localStorage.getItem("gabor")))
    return true
  }

  loadFromUrl() {
    if (!urlFragmentData || !urlFragmentData.team) return

    this._set(urlFragmentData.team)
    return true
  }

  // Private metastores

  get _costMetaStore() {
    return derived(
      [this.players, this.rerollCount, this.rerollCost, this.apothecary],
      ([$players, $rerollCount, $rerollCost, $apothecary]) =>
      derived($players.map(player => player.cost), $costs => {
        const playersTotalCost = $costs.reduce((sum, playerCost) => sum + playerCost, 0)
        const rerollsTotalCost = $rerollCount * $rerollCost
        const apothecaryTotalCost = $apothecary ? this.apothecaryCost : 0

        return playersTotalCost + rerollsTotalCost + apothecaryTotalCost
      })
    )
  }

  get _positionAvailabilitiesMetaStore() {
    return derived([this._positionMaxCounts, this.players], ([$positionMaxCounts, $players]) =>
      derived($players.map(player => player.positionId), $positionIds =>
        $positionIds.reduce((memo, positionId) => {
          if (positionId) memo[positionId] = memo[positionId] - 1
          return memo
        }, {...$positionMaxCounts})
      )
    )
  }

  // Private Methods (ES10 private methods are not well supported by browsers yet)

  _positionIds(rulebookRoster) {
    return Object.keys(rulebookRoster.positions)
  }

  get _positionMaxCounts() {
    return derived(this.rulebookRoster, $rulebookRoster =>
      this._positionIds($rulebookRoster).reduce((memo, positionId) => {
        memo[positionId] = $rulebookRoster.positions[positionId].maxNumber
        return memo
      }, {})
    )
  }

  _set(values) {
    if (values.hasOwnProperty("name")) this.name.set(values.name)
    if (values.hasOwnProperty("coach")) this.coach.set(values.coach)
    if (values.hasOwnProperty("race")) this.race.set(values.race)
    if (values.hasOwnProperty("rerollCount")) this.rerollCount.set(values.rerollCount)
    if (values.hasOwnProperty("apothecary")) this.apothecary.set(values.apothecary)

    if (values.hasOwnProperty("players")) {
      this.players.set([])
      values.players.map(playerJson => this.addPlayer(playerJson))
    }
  }
}
