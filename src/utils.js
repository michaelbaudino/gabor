export const noop = ((..._args) => {})

export const watchable = (storeType, initialValue, callback) => {
  const { subscribe, update, set } = storeType(initialValue);

  return {
    subscribe,
    update: (updater) => {
      let newValue = undefined
      update((previousValue) => (newValue = updater(previousValue)))
      callback(newValue)
    },
    set: (newValue) => {
      set(newValue)
      callback(newValue)
    }
  }
}

export const onMetaStoreChange = (outerStore, callback) => {
	let innerStoreUnsubscribe = noop

	outerStore.subscribe(innerStore => {
		innerStoreUnsubscribe()
		innerStoreUnsubscribe = innerStore.subscribe($value => callback($value))
	})
}

export const chunkArray = (array, itemsPerChunk) =>
  array.reduce((result, item, index) => {
    const chunkIndex = Math.floor(index / itemsPerChunk)
    if (!result[chunkIndex]) result[chunkIndex] = [] // start a new chunk
    result[chunkIndex].push(item)
    return result
  }, [])

export const urlFragmentData = (function() {
  if (!document.location.hash.startsWith("#!")) return
  const base64 = document.location.hash.replace("#!", "")

  try {
    return JSON.parse(atob(base64))
  } catch {
    console.log("Error: invalid base64-encoded JSON in URL fragment.")
  }
})()

export function shareableURL(team) {
  const url = new URL(document.location.href)
  const urlFragmentData = { team: team }

  url.hash = "#!" + btoa(JSON.stringify(urlFragmentData))
  return url.toString()
}
