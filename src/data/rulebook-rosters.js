import amazons from "./rulebook-rosters/amazons.json"
import blackOrcs from "./rulebook-rosters/black-orcs.json"
import darkElves from "./rulebook-rosters/dark-elves.json"
import dwarfs from "./rulebook-rosters/dwarfs.json"
import lizardmen from "./rulebook-rosters/lizardmen.json"
import skavens from "./rulebook-rosters/skavens.json"

export default {
  amazons,
  blackOrcs,
  darkElves,
  dwarfs,
  lizardmen,
  skavens
}
