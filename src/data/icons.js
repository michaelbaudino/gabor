// Heroicons
// Source code: https://github.com/tailwindlabs/heroicons
// License (MIT): https://github.com/tailwindlabs/heroicons/blob/master/LICENSE

export const plusIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
</svg>
`

export const xIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
</svg>
`

export const banIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636" />
</svg>
`

export const warningIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
</svg>
`
export const menuIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
</svg>
`

export const chevronDoubleLeft = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
</svg>
`

export const loginIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" />
</svg>
`

export const logoutIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
</svg>
`

export const sparklesIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z" />
</svg>
`

export const shareIcon = `
<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z" />
</svg>
`

// Noto Emojis (version Unicode 13.1)
// Source code: https://github.com/googlefonts/noto-emoji/blob/v2020-09-16-unicode13_1
// License (Apache 2): https://github.com/googlefonts/noto-emoji/blob/v2020-09-16-unicode13_1/LICENSE

export const footballIcon = `
<svg enable-background="new 0 0 128 128" viewBox="0 0 128 128" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><linearGradient id="p" x1="25.915" x2="103.91" y1="26.112" y2="103.11" gradientUnits="userSpaceOnUse"><stop stop-color="#945D4F" offset=".3599"/><stop stop-color="#66352A" offset=".7899"/></linearGradient><path d="m115.68 11.9c-10.1-9.18-57.09-17.19-89.37 17.47s-20.52 80.5-10.41 89.68c10.1 9.17 58.82 7.5 85.62-21.29 26.81-28.78 24.26-76.68 14.16-85.86z" fill="url(#p)"/><defs><ellipse id="g" transform="matrix(.7071 -.7071 .7071 .7071 -26.51 64)" cx="64" cy="64" rx="62.38" ry="39.55"/></defs><clipPath><use xlink:href="#g"/></clipPath><defs><path id="f" d="m115.68 11.9c-10.1-9.18-57.09-17.19-89.37 17.47s-20.52 80.5-10.41 89.68c10.1 9.17 58.82 7.5 85.62-21.29 26.81-28.78 24.26-76.68 14.16-85.86z"/></defs><clipPath id="a"><use xlink:href="#f"/></clipPath><g clip-path="url(#a)"><linearGradient id="e" x1="77.657" x2="115.81" y1="8.0459" y2="46.203" gradientUnits="userSpaceOnUse"><stop stop-color="#F5F5F5" offset=".2078"/><stop stop-color="#E0E0E0" offset=".554"/></linearGradient><path d="m123.62 63.63c-2.14 0-2 1.44-2.74-0.71-8.05-23.17-30.28-45.88-51.93-53.72l-7.44-3.27c-2.76-1.28 0.96-2.46 2.24-5.22s4.56-3.96 7.32-2.67l2.53 1.16c24.75 11.51 44.31 31.66 55.08 56.74 1.2 2.8-0.09 6.04-2.89 7.24-0.71 0.31-1.45 0.45-2.17 0.45z" fill="url(#e)"/></g><g clip-path="url(#a)"><linearGradient id="d" x1="9.1518" x2="47.309" y1="76.551" y2="114.71" gradientUnits="userSpaceOnUse"><stop stop-color="#F5F5F5" offset=".2078"/><stop stop-color="#E0E0E0" offset=".554"/></linearGradient><path d="m59.01 128.23c-0.71 0-1.43-0.14-2.12-0.43-25.43-10.63-45.58-30.36-56.74-55.57l-1.05-2.38c-1.23-2.78 0.03-6.04 2.81-7.27s4.2-5.28 5.43-2.5l2.89 7.69c10.01 22.62 28.09 40.33 50.91 49.87 2.81 1.17 4.13 4.4 2.96 7.21-0.89 2.11-2.94 3.38-5.09 3.38z" fill="url(#d)"/></g><path d="m117.75 11.5s-14 30.25-42.75 61.75-60 48.5-60 48.5" clip-path="url(#a)" fill="none" stroke="#212121" stroke-miterlimit="10" stroke-width="4"/><path d="m49 97.5s15-11.25 28-24.75c12.75-13.25 20.25-26.25 20.25-26.25" fill="none" stroke="#ECEFF0" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="6"/><linearGradient id="c" x1="45.851" x2="71.513" y1="75.443" y2="101.1" gradientTransform="matrix(.9976 .0693 -.0693 .9976 6.5495 -3.707)" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m65.89 100.14c-0.89-0.06-1.76-0.47-2.4-1.19l-12.3-14.14c-1.27-1.46-1.12-3.67 0.34-4.94s3.67-1.12 4.94 0.34l12.3 14.14c1.27 1.46 1.12 3.67-0.34 4.94-0.73 0.63-1.65 0.91-2.54 0.85z" fill="url(#c)"/><linearGradient id="w" x1="57.213" x2="82.875" y1="65.975" y2="91.637" gradientTransform="matrix(.9996 .027 -.027 .9996 2.2406 -1.8088)" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m77.53 90.28c-0.9-0.02-1.78-0.39-2.45-1.09l-12.88-13.61c-1.33-1.4-1.27-3.62 0.13-4.95s3.62-1.27 4.95 0.13l12.89 13.6c1.33 1.4 1.27 3.62-0.13 4.95-0.71 0.67-1.61 0.99-2.51 0.97z" fill="url(#w)"/><linearGradient id="v" x1="68.337" x2="93.999" y1="54.229" y2="79.891" gradientTransform="matrix(.9978 -.0663 .0663 .9978 -4.4427 5.4716)" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m89.43 77.67c-0.89 0.06-1.81-0.22-2.54-0.86l-14.09-12.34c-1.45-1.27-1.6-3.48-0.33-4.94 1.27-1.45 3.48-1.6 4.94-0.33l14.1 12.34c1.45 1.27 1.6 3.48 0.33 4.94-0.64 0.73-1.51 1.13-2.41 1.19z" fill="url(#v)"/><linearGradient id="u" x1="78.401" x2="104.06" y1="42.374" y2="68.035" gradientTransform="matrix(.9951 -.0985 .0985 .9951 -5.4787 9.1213)" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m99.53 65.47c-0.89 0.09-1.82-0.16-2.56-0.78l-14.5-11.88c-1.5-1.23-1.71-3.43-0.49-4.93 1.23-1.49 3.43-1.71 4.93-0.49l14.49 11.88c1.5 1.23 1.71 3.43 0.49 4.93-0.62 0.75-1.47 1.18-2.36 1.27z" fill="url(#u)"/><linearGradient id="t" x1="80.608" x2="134.42" y1="-88.893" y2="-35.083" gradientUnits="userSpaceOnUse"><stop stop-color="#945D4F" offset=".3599"/><stop stop-color="#66352A" offset=".7899"/></linearGradient><path d="m159.55-106.74c-0.31-2.79-2.51-4.99-5.3-5.3-13.8-1.53-50.79 0.16-73.71 23.08-22.92 22.91-24.62 59.91-23.09 73.7 0.31 2.79 2.51 4.99 5.3 5.3 13.8 1.53 50.79-0.16 73.71-23.08 22.92-22.91 24.62-59.91 23.09-73.7z" fill="url(#t)"/><defs><ellipse id="s" transform="matrix(.7071 -.7071 .7071 .7071 74.913 58.854)" cx="108.5" cy="-61" rx="62.38" ry="39.55"/></defs><clipPath><use xlink:href="#s"/></clipPath><defs><ellipse id="r" transform="matrix(.7071 -.7071 .7071 .7071 74.913 58.854)" cx="108.5" cy="-61" rx="62.38" ry="39.55"/></defs><clipPath><use xlink:href="#r"/></clipPath><g opacity=".2"><ellipse transform="matrix(.7071 -.7071 .7071 .7071 74.913 58.854)" cx="108.5" cy="-61" rx="62.38" ry="39.55" fill="none"/></g><defs><ellipse id="q" transform="matrix(.7071 -.7071 .7071 .7071 74.913 58.854)" cx="108.5" cy="-61" rx="62.38" ry="39.55"/></defs><clipPath><use xlink:href="#q"/></clipPath><defs><ellipse id="o" transform="matrix(.7071 -.7071 .7071 .7071 74.913 58.854)" cx="108.5" cy="-61" rx="62.38" ry="39.55"/></defs><clipPath><use xlink:href="#o"/></clipPath><defs><path id="n" d="m159.55-106.74c-0.31-2.79-2.51-4.99-5.3-5.3-13.8-1.53-50.79 0.16-73.71 23.08-22.92 22.91-24.62 59.91-23.09 73.7 0.31 2.79 2.51 4.99 5.3 5.3 13.8 1.53 50.79-0.16 73.71-23.08 22.92-22.91 24.62-59.91 23.09-73.7z"/></defs><clipPath id="b"><use xlink:href="#n"/></clipPath><g clip-path="url(#b)"><linearGradient id="m" x1="123.65" x2="158.28" y1="-114.65" y2="-80.024" gradientUnits="userSpaceOnUse"><stop stop-color="#F5F5F5" offset=".2078"/><stop stop-color="#E0E0E0" offset=".554"/></linearGradient><path d="m164.82-63.67c-1.94 0-3.78-1.13-4.6-3.03-8.8-20.5-24.79-36.97-45.01-46.37l-2.29-1.07c-2.5-1.16-3.59-4.14-2.42-6.64 1.16-2.5 4.14-3.59 6.64-2.43l2.29 1.07c22.46 10.45 40.21 28.73 49.99 51.49 1.09 2.54-0.08 5.48-2.62 6.57-0.66 0.28-1.33 0.41-1.98 0.41z" fill="url(#m)"/></g><g clip-path="url(#b)"><linearGradient id="l" x1="55.17" x2="89.798" y1="-46.172" y2="-11.544" gradientUnits="userSpaceOnUse"><stop stop-color="#F5F5F5" offset=".2078"/><stop stop-color="#E0E0E0" offset=".554"/></linearGradient><path d="m101.15 0c-0.64 0-1.3-0.12-1.93-0.39-23.08-9.65-41.36-27.56-51.48-50.43l-0.95-2.16c-1.12-2.53 0.02-5.48 2.55-6.6 2.52-1.12 5.48 0.02 6.6 2.55l0.95 2.16c9.08 20.52 25.49 36.6 46.2 45.25 2.55 1.07 3.75 3.99 2.68 6.54-0.81 1.92-2.67 3.08-4.62 3.08z" fill="url(#l)"/></g><path d="m157.17-110.83s-10.75 14.42-47.33 51-53.09 49.41-53.09 49.41" clip-path="url(#b)" fill="none" stroke="#212121" stroke-miterlimit="10" stroke-width="2"/><path d="m84.17-36.85s12.17-10.48 25.17-23.98 24-24.98 24-24.98" fill="none" stroke="#ECEFF0" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="6"/><g opacity=".2"><path d="m145.8-109.46c2.98 0 5.78 0.14 8.11 0.4 1.4 0.16 2.49 1.25 2.65 2.65 0.79 7.14 0.6 19.41-2.32 32.45-3.62 16.16-10.31 29.22-19.9 38.81-20.98 20.98-53.63 22.61-63.14 22.61-2.98 0-5.78-0.14-8.11-0.4-1.4-0.16-2.49-1.25-2.65-2.65-0.79-7.14-0.6-19.41 2.32-32.45 3.62-16.16 10.31-29.22 19.9-38.81 20.98-20.98 53.63-22.61 63.14-22.61m0-3c-17.05 0-46.09 4.32-65.26 23.5-22.92 22.91-24.62 59.91-23.09 73.7 0.31 2.79 2.51 4.99 5.3 5.3 2.25 0.25 5.12 0.41 8.44 0.41 17.05 0 46.09-4.32 65.26-23.5 22.92-22.92 24.62-59.91 23.08-73.71-0.31-2.79-2.51-4.99-5.3-5.3-2.24-0.24-5.11-0.4-8.43-0.4z" fill="#434343"/></g><linearGradient id="k" x1="79.331" x2="104.99" y1="-59.582" y2="-33.92" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m99.87-35.54c-0.9 0-1.79-0.34-2.48-1.03l-13.24-13.25c-1.37-1.37-1.37-3.58 0-4.95s3.58-1.37 4.95 0l13.25 13.25c1.37 1.37 1.37 3.58 0 4.95-0.69 0.69-1.58 1.03-2.48 1.03z" fill="url(#k)"/><linearGradient id="j" x1="89.971" x2="115.63" y1="-70.222" y2="-44.56" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m110.51-46.18c-0.9 0-1.79-0.34-2.48-1.03l-13.24-13.25c-1.37-1.37-1.37-3.58 0-4.95s3.58-1.37 4.95 0l13.25 13.25c1.37 1.37 1.37 3.58 0 4.95-0.69 0.69-1.58 1.03-2.48 1.03z" fill="url(#j)"/><linearGradient id="i" x1="100.61" x2="126.27" y1="-80.863" y2="-55.201" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m121.15-56.82c-0.9 0-1.79-0.34-2.47-1.03l-13.25-13.25c-1.37-1.37-1.37-3.58 0-4.95s3.58-1.37 4.95 0l13.25 13.25c1.37 1.37 1.37 3.58 0 4.95-0.69 0.69-1.58 1.03-2.48 1.03z" fill="url(#i)"/><linearGradient id="h" x1="111.25" x2="136.91" y1="-91.503" y2="-65.841" gradientUnits="userSpaceOnUse"><stop stop-color="#EFEBE9" offset=".331"/><stop stop-color="#BDAAA4" offset="1"/></linearGradient><path d="m131.79-67.46c-0.9 0-1.79-0.34-2.47-1.02l-13.25-13.25c-1.37-1.37-1.37-3.58 0-4.95s3.58-1.37 4.95 0l13.25 13.25c1.37 1.37 1.37 3.58 0 4.95-0.69 0.68-1.58 1.02-2.48 1.02z" fill="url(#h)"/><g opacity=".2"><path d="m85.02 7c13.66 0 24.74 3.58 28.64 7.12 2.47 2.24 5.54 9.3 6.06 21.82 0.73 17.52-4.24 42.42-20.4 59.77-17.38 18.67-44.44 25.29-62.33 25.29-10.67 0-16.85-2.16-19.07-4.18-4.22-3.82-9.03-15.86-9.65-30.62-0.49-11.82 1.48-34.65 20.23-54.79 19.79-21.24 43.9-24.41 56.52-24.41m0-3c-18.19 0-40.73 6.06-58.71 25.37-32.28 34.66-20.52 80.5-10.41 89.68 3.41 3.1 11.21 4.96 21.09 4.96 19.39 0 46.77-7.17 64.53-26.25 26.81-28.77 24.27-76.68 14.16-85.85-4.48-4.07-16.19-7.91-30.66-7.91z" fill="#434343"/></g></svg>
`

export const collisionIcon = `
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 128 128" style="enable-background:new 0 0 128 128;" xml:space="preserve">
<g>
	<polygon style="fill:#FFA000;stroke:#F44336;stroke-width:3;stroke-miterlimit:10;" points="68.27,8.51 84.11,41.67 119.44,9.79 
		101.38,55.43 121.05,50.12 102.4,76.63 119.43,82.5 102.03,90.77 118.96,119.8 86.77,99.89 79.87,121.43 69.65,98.4 40.65,120.18 
		49.47,95.33 9.5,103.37 39.42,81.1 7.89,58.4 43.85,61.51 10.6,10.52 64,45.51 	"/>
	<radialGradient id="SVGID_1_" cx="77.5873" cy="75.7353" r="26.3653" gradientUnits="userSpaceOnUse">
		<stop  offset="0" style="stop-color:#FFFDE7"/>
		<stop  offset="1" style="stop-color:#FFF176"/>
	</radialGradient>
	<polygon style="fill:url(#SVGID_1_);stroke:#FFEB3B;stroke-width:2;stroke-miterlimit:10;" points="72.63,34.77 82.17,59.48 
		101.05,38.17 86.86,67.91 104.07,62.74 88.12,78.59 101.48,82.98 88.68,84.87 98.87,99.31 82.31,88.73 78.5,101.05 72.63,86.96 
		57.98,98.12 64,84.45 41,89.47 57.95,79.19 35.84,68.56 60.33,70.84 40.22,39.29 71.1,60.17 	"/>
</g>
</svg>
`

export const coinIcon = `
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 128 128" style="enable-background:new 0 0 128 128;" xml:space="preserve">
<g>
	<circle style="fill:#D68F30;" cx="64" cy="66.58" r="57.36"/>
	<g>
		<g>
			<path style="fill:#BC6F00;" d="M10.54,81.48v5.86c0.6,1.55,1.27,3.08,2,4.56V81.48H10.54z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M17.08,92.51v7.04c0.8,1.13,1.63,2.24,2.5,3.31V92.51H17.08z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M25.74,101.71v7.59c0.97,0.87,1.97,1.7,3,2.5v-10.09H25.74z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M36.4,108.7v8.16c0.98,0.54,1.98,1.05,3,1.54v-9.7H36.4z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M48.49,113.15v8.64c0.99,0.28,1.99,0.53,3,0.76v-9.4H48.49z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M62.64,114.69v9.21c0.45,0.01,0.9,0.03,1.36,0.03c0.55,0,1.1-0.03,1.64-0.04v-9.2H62.64z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M76.79,113.08v9.4c1.01-0.23,2.01-0.49,3-0.77v-8.63H76.79z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M88.85,108.57v9.71c1.02-0.49,2.02-1.02,3-1.57v-8.14H88.85z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M99.48,101.52v10.1c1.03-0.81,2.03-1.64,3-2.52v-7.58H99.48z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M111.09,92.29h-2.5v10.35c0.87-1.08,1.71-2.19,2.5-3.33V92.29z"/>
		</g>
		<g>
			<path style="fill:#BC6F00;" d="M117.59,81.22h-2v10.44c0.73-1.51,1.4-3.06,2-4.63V81.22z"/>
		</g>
	</g>
	<circle style="fill:#FFF176;" cx="64" cy="61.42" r="57.36"/>
	<circle style="fill:#F2BC1A;" cx="64" cy="61.42" r="52.25"/>
	<g>
		<path style="fill:#E08F00;" d="M11.65,63.42c-0.37-6.88,0.82-13.86,3.22-20.4c2.5-6.52,6.33-12.55,11.16-17.67
			C35.73,15.09,49.81,9.14,64,9.07c14.19,0.08,28.28,6.02,37.96,16.29c4.84,5.11,8.66,11.15,11.16,17.66
			c2.41,6.55,3.6,13.52,3.22,20.4h-0.2c-0.02-6.85-1.38-13.68-4-20c-2.61-6.32-6.48-12.12-11.29-16.97
			c-4.82-4.85-10.59-8.75-16.9-11.38c-6.3-2.64-13.13-4-19.96-4c-6.83,0-13.66,1.36-19.96,4c-6.31,2.63-12.08,6.53-16.9,11.38
			c-4.82,4.85-8.68,10.65-11.29,16.97c-2.62,6.32-3.98,13.15-4,20H11.65z"/>
	</g>
	<path style="fill:#FFF176;" d="M64,4.07c-31.68,0-57.36,25.68-57.36,57.36c0,31.68,25.68,57.36,57.36,57.36
		s57.36-25.68,57.36-57.36C121.36,29.75,95.68,4.07,64,4.07z M64,113.68c-28.86,0-52.25-23.39-52.25-52.25
		C11.75,32.56,35.14,9.17,64,9.17s52.25,23.39,52.25,52.25C116.25,90.28,92.86,113.68,64,113.68z"/>
	<g>
		<g>
			<polygon style="fill:#D38200;" points="37.99,21.35 39.26,25.28 43.4,23.7 43.4,25.28 40.05,27.71 41.33,30.05 41.33,31.64 
				37.99,29.21 34.64,31.64 34.64,30.05 35.92,27.71 32.58,25.28 32.58,23.7 36.71,25.28 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="22.01,43.91 23.29,47.84 27.42,46.26 27.42,47.84 24.08,50.27 25.36,52.62 25.36,54.2 
				22.01,51.77 18.67,54.2 18.67,52.62 19.95,50.27 16.6,47.84 16.6,46.26 20.74,47.84 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="21.54,71.72 22.82,75.65 26.95,74.07 26.95,75.65 23.61,78.08 24.89,80.43 24.89,82.01 
				21.54,79.58 18.2,82.01 18.2,80.43 19.48,78.08 16.13,75.65 16.13,74.07 20.27,75.65 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="38.6,93.8 39.88,97.73 44.01,96.15 44.01,97.73 40.67,100.16 41.95,102.5 41.95,104.09 
				38.6,101.66 35.26,104.09 35.26,102.5 36.54,100.16 33.19,97.73 33.19,96.15 37.33,97.73 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="90.01,21.35 88.74,25.28 84.6,23.7 84.6,25.28 87.95,27.71 86.67,30.05 86.67,31.64 
				90.01,29.21 93.36,31.64 93.36,30.05 92.08,27.71 95.42,25.28 95.42,23.7 91.29,25.28 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="105.99,43.91 104.71,47.84 100.58,46.26 100.58,47.84 103.92,50.27 102.64,52.62 
				102.64,54.2 105.99,51.77 109.33,54.2 109.33,52.62 108.05,50.27 111.4,47.84 111.4,46.26 107.26,47.84 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="106.46,71.72 105.18,75.65 101.05,74.07 101.05,75.65 104.39,78.08 103.11,80.43 
				103.11,82.01 106.46,79.58 109.8,82.01 109.8,80.43 108.52,78.08 111.87,75.65 111.87,74.07 107.73,75.65 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="89.4,93.8 88.12,97.73 83.99,96.15 83.99,97.73 87.33,100.16 86.05,102.5 86.05,104.09 
				89.4,101.66 92.74,104.09 92.74,102.5 91.46,100.16 94.81,97.73 94.81,96.15 90.67,97.73 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon style="fill:#FFF176;" points="89.4,92.21 90.67,96.14 94.81,96.14 91.46,98.57 92.74,102.5 89.4,100.07 86.05,102.5 
				87.33,98.57 83.99,96.14 88.12,96.14 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="106.46,70.14 107.73,74.07 111.87,74.07 108.52,76.5 109.8,80.43 106.46,78 103.11,80.43 
				104.39,76.5 101.05,74.07 105.18,74.07 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="105.99,42.33 107.26,46.26 111.4,46.26 108.05,48.69 109.33,52.62 105.99,50.19 
				102.64,52.62 103.92,48.69 100.58,46.26 104.71,46.26 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="90.01,19.76 91.29,23.69 95.42,23.69 92.08,26.12 93.36,30.05 90.01,27.62 86.67,30.05 
				87.95,26.12 84.6,23.69 88.74,23.69 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon style="fill:#D38200;" points="64.05,102.5 65.33,106.43 69.46,104.85 69.46,106.43 66.12,108.86 67.4,111.21 
				67.4,112.79 64.05,110.36 60.71,112.79 60.71,111.21 61.99,108.86 58.64,106.43 58.64,104.85 62.78,106.43 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="64.05,100.4 65.33,104.33 69.46,104.33 66.12,106.76 67.4,110.69 64.05,108.26 
				60.71,110.69 61.99,106.76 58.64,104.33 62.78,104.33 			"/>
		</g>
		<g>
			<polygon style="fill:#D38200;" points="64.05,12.89 65.33,16.82 69.46,15.24 69.46,16.82 66.12,19.25 67.4,21.6 67.4,23.18 
				64.05,20.75 60.71,23.18 60.71,21.6 61.99,19.25 58.64,16.82 58.64,15.24 62.78,16.82 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="64.05,11.31 65.33,15.24 69.46,15.24 66.12,17.67 67.4,21.6 64.05,19.17 60.71,21.6 
				61.99,17.67 58.64,15.24 62.78,15.24 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon style="fill:#FFF176;" points="38.6,92.21 37.33,96.14 33.19,96.14 36.54,98.57 35.26,102.5 38.6,100.07 41.95,102.5 
				40.67,98.57 44.01,96.14 39.88,96.14 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="21.54,70.14 20.27,74.07 16.13,74.07 19.48,76.5 18.2,80.43 21.54,78 24.89,80.43 
				23.61,76.5 26.95,74.07 22.82,74.07 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="22.01,42.33 20.74,46.26 16.6,46.26 19.95,48.69 18.67,52.62 22.01,50.19 25.36,52.62 
				24.08,48.69 27.42,46.26 23.29,46.26 			"/>
		</g>
		<g>
			<polygon style="fill:#FFF176;" points="37.99,19.76 36.71,23.69 32.58,23.69 35.92,26.12 34.64,30.05 37.99,27.62 41.33,30.05 
				40.05,26.12 43.4,23.69 39.26,23.69 			"/>
		</g>
	</g>
	<path style="fill:#D38200;" d="M95.22,48.2c0-0.35-62.38,0-62.38,0l-0.56,1.68l0,2.87c0,0.52,0.42,0.94,0.94,0.94h30.99v0h30.57
		c0.52,0,0.94-0.42,0.94-0.94v-2.79L95.22,48.2z"/>
	<path style="fill:#D38200;" d="M95.31,85.5l-62.21,0l-4.16,2.09l0.05,2.69c0.01,0.29,0.25,0.53,0.55,0.53l68.93,0
		c0.29,0,0.54-0.23,0.55-0.53l0.05-2.64L95.31,85.5z"/>
	<g>
		<g>
			<path style="fill:#D38200;" d="M43.13,77.32h-3.58c-0.59,0-1.07-1.86-1.07-2.45l5.71-0.18C44.19,75.28,43.71,77.32,43.13,77.32z"
				/>
			<path style="fill:#D38200;" d="M44.35,58.01h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.47-3.93h8.83l-0.4,3.93
				C45.19,57.71,44.8,58.01,44.35,58.01z"/>
			<path style="fill:#D38200;" d="M38.32,78.51h6.03c0.45,0,1.34,0.28,1.46,0.71c0,0-0.03,1.87-0.03,2.61s-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-0.97-0.6-0.97-1.28s-0.03-2.59-0.03-2.59C36.99,78.81,37.87,78.51,38.32,78.51z"/>
		</g>
	</g>
	<g>
		<g>
			<path style="fill:#D38200;" d="M89.63,77.32h-3.58c-0.59,0-1.07-1.86-1.07-2.45l5.71-0.18C90.69,75.28,90.22,77.32,89.63,77.32z"
				/>
			<path style="fill:#D38200;" d="M90.85,58.01h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.47-3.93h8.83l-0.4,3.93
				C91.69,57.71,91.3,58.01,90.85,58.01z"/>
			<path style="fill:#D38200;" d="M84.82,78.51h6.03c0.45,0,1.34,0.28,1.46,0.71c0,0-0.03,1.87-0.03,2.61s-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-0.97-0.6-0.97-1.28s-0.03-2.59-0.03-2.59C83.49,78.81,84.37,78.51,84.82,78.51z"/>
		</g>
	</g>
	<g>
		<g>
			<path style="fill:#D38200;" d="M74.13,77.32h-3.58c-0.59,0-1.07-1.86-1.07-2.45l5.71-0.18C75.19,75.28,74.72,77.32,74.13,77.32z"
				/>
			<path style="fill:#D38200;" d="M75.35,58.01h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.47-3.93h8.83l-0.4,3.93
				C76.19,57.71,75.8,58.01,75.35,58.01z"/>
			<path style="fill:#D38200;" d="M69.32,78.51h6.03c0.45,0,1.34,0.28,1.46,0.71c0,0-0.03,1.87-0.03,2.61s-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-0.97-0.6-0.97-1.28s-0.03-2.59-0.03-2.59C67.99,78.81,68.87,78.51,69.32,78.51z"/>
		</g>
	</g>
	<g>
		<g>
			<path style="fill:#D38200;" d="M58.63,77.32h-3.58c-0.59,0-1.07-1.86-1.07-2.45l5.71-0.18C59.69,75.28,59.22,77.32,58.63,77.32z"
				/>
			<path style="fill:#D38200;" d="M59.85,58.01h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.47-3.93h8.83l-0.4,3.93
				C60.69,57.71,60.3,58.01,59.85,58.01z"/>
			<path style="fill:#D38200;" d="M53.82,78.51h6.03c0.45,0,1.34,0.28,1.46,0.71c0,0-0.03,1.87-0.03,2.61s-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-0.97-0.6-0.97-1.28s-0.03-2.59-0.03-2.59C52.49,78.81,53.37,78.51,53.82,78.51z"/>
		</g>
	</g>
	<path style="fill:#FFF176;" d="M95.67,45.52c0-0.35-0.18-0.68-0.45-0.84l-31.18-17.7L32.82,44.66c-0.3,0.18-0.49,0.51-0.49,0.86
		l-0.05,4.37c0,0.55,0.45,1,1,1h61.43c0.55,0,1-0.45,1-1L95.67,45.52z"/>
	<polygon style="fill:#F2BC1A;" points="87.84,44.33 64,30.8 40.16,44.33 	"/>
	<polygon style="fill:#FFF176;" points="79.07,42.68 64,34.12 48.93,42.68 	"/>
	<path style="fill:#FFF176;" d="M93.39,82.02H34.61c-0.58,0-1.13,0.25-1.52,0.68l-4.04,4.51c-0.28,0.31-0.06,0.8,0.36,0.8h69.18
		c0.41,0,0.63-0.49,0.36-0.8L94.9,82.7C94.52,82.27,93.96,82.02,93.39,82.02z"/>
	<g>
		<g>
			<path style="fill:#FFF176;" d="M89.63,75.51h-3.58c-0.59,0-1.07-0.48-1.07-1.07V58.46c0-0.59,0.48-1.07,1.07-1.07h3.58
				c0.59,0,1.07,0.48,1.07,1.07v15.99C90.7,75.04,90.22,75.51,89.63,75.51z"/>
			<path style="fill:#FFF176;" d="M90.86,56.2h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.46-1.59c-0.19-0.64,0.3-1.28,0.97-1.28h6.95
				c0.67,0,1.15,0.64,0.97,1.28l-0.46,1.59C91.7,55.9,91.3,56.2,90.86,56.2z"/>
			<path style="fill:#FFF176;" d="M84.82,76.71h6.03c0.45,0,0.84,0.3,0.97,0.73l0.46,1.59c0.19,0.64-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-1.15-0.64-0.97-1.28l0.46-1.59C83.98,77,84.38,76.71,84.82,76.71z"/>
		</g>
		<g>
			<path style="fill:#FFF176;" d="M43.13,75.51h-3.58c-0.59,0-1.07-0.48-1.07-1.07V58.46c0-0.59,0.48-1.07,1.07-1.07h3.58
				c0.59,0,1.07,0.48,1.07,1.07v15.99C44.19,75.04,43.71,75.51,43.13,75.51z"/>
			<path style="fill:#FFF176;" d="M44.35,56.2h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.46-1.59c-0.19-0.64,0.3-1.28,0.97-1.28h6.95
				c0.67,0,1.15,0.64,0.97,1.28l-0.46,1.59C45.19,55.9,44.8,56.2,44.35,56.2z"/>
			<path style="fill:#FFF176;" d="M38.32,76.71h6.03c0.45,0,0.84,0.3,0.97,0.73l0.46,1.59c0.19,0.64-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-1.15-0.64-0.97-1.28l0.46-1.59C37.48,77,37.87,76.71,38.32,76.71z"/>
		</g>
		<g>
			<path style="fill:#FFF176;" d="M58.63,75.51h-3.58c-0.59,0-1.07-0.48-1.07-1.07V58.46c0-0.59,0.48-1.07,1.07-1.07h3.58
				c0.59,0,1.07,0.48,1.07,1.07v15.99C59.69,75.04,59.22,75.51,58.63,75.51z"/>
			<path style="fill:#FFF176;" d="M59.85,56.2h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.46-1.59c-0.19-0.64,0.3-1.28,0.97-1.28h6.95
				c0.67,0,1.15,0.64,0.97,1.28l-0.46,1.59C60.69,55.9,60.3,56.2,59.85,56.2z"/>
			<path style="fill:#FFF176;" d="M53.82,76.71h6.03c0.45,0,0.84,0.3,0.97,0.73l0.46,1.59c0.19,0.64-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-1.15-0.64-0.97-1.28l0.46-1.59C52.98,77,53.37,76.71,53.82,76.71z"/>
		</g>
		<g>
			<path style="fill:#FFF176;" d="M74.13,75.51h-3.58c-0.59,0-1.07-0.48-1.07-1.07V58.46c0-0.59,0.48-1.07,1.07-1.07h3.58
				c0.59,0,1.07,0.48,1.07,1.07v15.99C75.19,75.04,74.72,75.51,74.13,75.51z"/>
			<path style="fill:#FFF176;" d="M75.35,56.2h-6.03c-0.45,0-0.84-0.3-0.97-0.73l-0.46-1.59c-0.19-0.64,0.3-1.28,0.97-1.28h6.95
				c0.67,0,1.15,0.64,0.97,1.28l-0.46,1.59C76.19,55.9,75.8,56.2,75.35,56.2z"/>
			<path style="fill:#FFF176;" d="M69.32,76.71h6.03c0.45,0,0.84,0.3,0.97,0.73l0.46,1.59c0.19,0.64-0.3,1.28-0.97,1.28h-6.95
				c-0.67,0-1.15-0.64-0.97-1.28l0.46-1.59C68.48,77,68.88,76.71,69.32,76.71z"/>
		</g>
	</g>
</g>
</svg>
`
