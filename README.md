# 🏈 G.A.B.O.R 💥

> Gabor's Another BloodBowl Online Roster

## Get started

Install the dependencies…

```shell
npm install
```

… then start in development mode:

```shell
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running.

## Building and running in production mode

To create an optimised version of the app:

```shell
npm run build
```

You can run the newly built app with `npm run start`.

## Roadmap

* [ ] Roster creation:
  * [x] team players
  * [x] costs
  * [x] player limit
  * [x] skills (default and additional)
  * [x] team features (re-rolls, apothecary, …)
  * [ ] all rosters data
  * [ ] allow editing player number / reordering players
* [x] Responsive display
* [x] [Netlify](https://www.netlify.com) deployment
* [x] I18n
* [x] Save/load teams in LocalStorage
* [x] Share team (via URL fragment)
* [ ] Team printable (PDF) export
* [ ] Team & player cards generation
* [ ] [PostgREST](https://postgrest.org) backend ([with Auth0 JWT](https://samkhawase.com/blog/postgrest/postgrest_auth0_service)) to:
  * [ ] Save teams on server
  * [ ] Share teams by unique link
* [ ] League / Tournament management
* [ ] Online game (may need a more advanced backend than PostgREST, maybe Rails or Scala…)
