import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// See https://vitejs.dev/config/

export default defineConfig({
  plugins: [
    svelte({
      hot: {
        preserveLocalState: true
      }
    })
  ]
})
